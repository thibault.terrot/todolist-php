
<?php
include "header.php";
require_once "autoload.php";

if(isset($_POST['Déconnexion']))
{
    unset($_SESSION["user"]);
    echo '<div class="alert alert-warning" role="alert">Vous avez été déconnecté</div>';
}

if(isset($_SESSION['user']) && isset($_SESSION['role']))
{
    switch ($_SESSION['role']) {
        case "ROLE_ADMIN":
                header("Location: index.php");
            break;

        case "ROLE_USER":
            header("Location: index.php");
            break;

        default:
            break;
    }
}


class Connexion{
    // Authentification, retourne l'objet membre correspondant au (login, mot de passe) ou NULL
    public function checkAuthentification($login, $mdp)
    {
        $pdo = MaBD::getInstance();
        $requete = $pdo->query("Select idMembre, identifiant, motDePasse FROM membre where identifiant=".$login." LIMIT 1;");
        $user = $requete->fetchAll(PDO::FETCH_ASSOC);

        if($user->mdp == $mdp)
            return $user;
        return NULL;
    }
}



// Préparation des paramètres
   $param['user'] = isset($_POST['user'])?$_POST['user']:"";
   $param['mdp'] = isset($_POST['mdp'])?$_POST['mdp']:"";
   $param['erreur'] = false;
   $param['message'] = "";

   if (isset($_POST['Connect'])) // On a cliqué sur le bouton
    {
        if ($param['user'] == '' || $param['mdp'] == '')
        {
            $param['erreur'] = true;
            $param['message'] = "Un identifiant et un mot de passe correct sont obligatoire pour continuer";
        }
        else
        {
            // Recherche de l'identification dans la base
            $pdo = MaBD::getInstance();
            $requete = $pdo->query("Select * FROM membre where identifiant='".$param['user']."' LIMIT 1;");
            $user = $requete->fetchAll(PDO::FETCH_ASSOC); 
            $inscrit = NULL;

            foreach($user as $line){
                if($line['motDePasse'] == $param['mdp']) {
                    $inscrit = $user;
                    $param['nom'] = $line['nom'];
                    $param['idMembre'] = $line['idMembre'];
                    $role = $line['role'];
                }
            }

            if ($inscrit === NULL)
            {
               $param['erreur'] = true;
               $param['message'] = "Erreur d'authentification, mot de passe ou identifiant incorrect.";
            }
            else
            {
                //$inscrit->motDePasse = "";
                $_SESSION['user'] = $param['nom'];
                $_SESSION['role'] = $role;
                $_SESSION['idMembre'] = $param['idMembre'];

                switch ($role) {
                    case "ROLE_ADMIN":
                        header("Location: index.php");
                        break;

                    case "ROLE_USER":
                        header("Location: index.php");
                        break;

                    default:
                        break;
                }
                exit(0);
            }
        }
   }
?>

<?php
    include "menu_todolist.php";
    include "class/utilClass.php";
?>

<html>
    <head>
        <title>Page de connexion</title>
    </head>
    <body>
        <div class="container">
            <h1>Page de connexion</h1>
            <br/>
            <?php
                if ($param['erreur'])
                    echo '<div class="alert alert-danger" role="alert"><p id ="msg" class="erreur">', $param['message'], '</p></div>';
                else{
                    if($param['message'])
                        echo '<div class="alert alert-warning" role="alert"><p class="normal">', $param['message'], '</p></div>';
                }
            ?>
            <br/>
            <br/>
            <form id="formLogin" method="post">
                <table><tbody>
                    <tr>
                        <th>Identifiant : </th>
                        <td><input type="text" name="user" size="18" value="<?php echo $param['user']; ?>"/></td>
                    </tr>
                    <tr>
                        <th>Mot de passe : </th>
                        <td><input type="password" name="mdp" size="18" value="<?php echo $param['mdp']; ?>"/></td>
                    </tr>
                    <tr><td colspan="2" style="text-align: center; border: none;">
                    <button type="submit" class="btn btn-primary" name="Connect">Connexion</button>
                    </tr>
                </tbody></table>
            </form>
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>


