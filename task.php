<?php
    require_once "header.php";
    require_once "menu_todolist.php";
    require_once "class/utilClass.php";
?>

<html>
    <body>
        <div class="container">
            <h1>Toutes les tâches</h1>
            <br/>
            <div class="alert alert-info" role="alert">
                Ici sont affichées toutes les tâches publiques. Les tâches rendues privées par leurs créateurs ne sont pas affichées.
            </div>
            <br/>
            
            <table class="table table-striped task-table">
            <?php
                $array_task = Util::getAllPublicTask();
                echo '<table class="table table-striped task-table"><tr><th>Nom</th><th>Description</th><th>Date de début</th><th>Date de fin</th><th>Projet</th><th>Créateur</th><th>Date de création</th></tr>';
                foreach ($array_task as $task){
                    echo '<tr><td>'.$task->getNom().'</td><td>'.$task->getDescription().'</td><td>'.$task->getDateDebut().'</td><td>'.$task->getDateLimite().'</td><td>'.$task->getProjet().'</td><td>'.$task->getCreateur()->getNom().'</td><td>'.$task->getDateCreation().'</td></tr>';
                }
                echo '</table>';
            ?>
            </table>
            <br/>
            <br/>
            <?php 
                if(isset($_SESSION['user']))
                {
                    echo '<h1>Ajouter une tâche</h1>
                    <form action="task-add.php" method="POST" enctype="multipart/form-data">
                        <p>Nom: <input type="text" name="nom"/></p>
                        <input style="display:none;" type="text" name="idMembre" value="'.$_SESSION['idMembre'].'"/>
                        <select name="projets" id="projet-select">
                            <option value="0">Pas de projet</option>';

                    $requete = Util::getAllPublicProject();
                    foreach ($requete as $row){
                        echo '<option value="'.$row['idCompte'].'">'.$row['nom'].'</option>';
                    }

                    echo '</select>
                    <p>Publique: <input type="checkbox" name="public" /></p>
                    <p>Description: <br /><textarea name="description" rows="10"
                    cols="50"></textarea></p>
                    <input type="date" name="dateDebut" value="'.date('d/m/Y').'">
                    <input type="date" name="dateLimite" value="'.date('d/m/Y').'">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2097152">
                    <p>Choisissez une photo avec une taille inférieure à 2 Mo.</p>
                    <input type="file" name="photo">
                    <br/><br/>
                    <button type="submit" class="btn btn-primary">Créer tâche <i class="fas fa-plus-square"></i></button>
                </form>';
                    
                }else{
                    echo '<div class="alert alert-info" role="alert">Veuillez vous connecter pour créer une tâche</div>';
                }
            ?>

                
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>

