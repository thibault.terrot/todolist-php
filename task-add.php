<?php
    require_once "class/tacheClass.php";
    require_once "class/utilClass.php";

    /* if ($_FILES['photo']['error']) {
        switch ($_FILES['photo']['error']){
        case 1: // UPLOAD_ERR_INI_SIZE
            echo "La taille du fichier est plus grande que la limite
            autorisée.";
            break;
        case 2: // UPLOAD_ERR_FORM_SIZE
            echo "La taille du fichier est plus grande que la limite
            autorisée.";
            break;
        case 3: // UPLOAD_ERR_PARTIAL
            echo "L'envoi du fichier a été interrompu pendant le
            transfert.";
            break;
        case 4: // UPLOAD_ERR_NO_FILE
            echo "La taille du fichier que vous avez envoyé est nulle.";
            break;
        }
    }
    else {
        //s'il n'y a pas d'erreur alors $_FILES['nom_du_fichier']['error']
        //vaut 0
        if ((isset($_FILES['photo']['name'])&&($_FILES['photo']['error'] == UPLOAD_ERR_OK))) {
            $chemin_destination = 'photos/';
            //déplacement du fichier du répertoire temporaire (stocké
            //par défaut) dans le répertoire de destination
            move_uploaded_file($_FILES['photo']['tmp_name'],
            $chemin_destination.$_FILES['photo']['name']);
        }
    } */
    $Err = array();
    if (empty($_POST["nom"])) {
        $Err[] = "- Veuillez remplir le nom de la tâche.";
    } else {
        $nom = $_POST["nom"];
    }
    if (empty($_POST["description"])) {
        $description = False;      
    } else {
        $description = $_POST["description"];
    }
    if (empty($_POST["dateDebut"])) {
        $dateDebut = False;
    } else {
        $dateDebut = $_POST["dateDebut"];
    }
    if (empty($_POST["dateLimite"])) {
        $dateLimite = False;
    } else {
        $dateLimite = $_POST["dateLimite"];
    }
    if (empty($_POST["projets"])) {
        $projet = NULL;
    } else {
        $projet = $_POST["projets"];
    }
    if (empty($_POST["public"])) {
        $public = False;
    } else {
        $public = $_POST["public"];
    }

    $createur = Util::getCurrentMembre();
    if(count($Err)==0){ //Aucune erreur
        $tache = new Tache($nom, $description, $dateDebut, $dateLimite, date("Y-m-d"), $public, $createur, $projet);
        $save = $tache->saveBDD();
        require("task.php");
    }
    else{
        $erreurs = "";
        foreach ($Err as $er){
            $erreurs .="</br>&nbsp;&nbsp;&nbsp;&nbsp;".$er;
        }
        echo "<div class='alert alert-danger' role='alert'>La tache n'a pas pu être ajoutée: ".$erreurs."</div>";
    }

?>
