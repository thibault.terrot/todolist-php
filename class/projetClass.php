<?php
    require_once "MaBD.php";

    Class Projet{
        public function __construct(string $nom, string $description, string $dateDebut, string $dateLimite, bool $public){
            $this->description = $description;
            $newDateDebut = date("Y-m-d", strtotime($dateDebut));  
            $this->dateDebut = $newDateDebut;
            $newDateLimite = date("Y-m-d", strtotime($dateLimite));  
            $this->dateLimite = $newDateLimite;
            $this->nom = $nom;
            $this->public = $public;
        }

        public $idProjet = "";
        public $nom = "";
        public $description = "";
        public $dateDebut = "";
        public $public = False;
        public $dateLimite = "";
        #private $taches[tache] = "";

        public function getId(){
            return $this->idProjet;
        }

        public function getDescription(){
            return $this->description;
        }

        public function getDateDebut(){
            return $this->dateDebut;
        }

        public function getDateLimite(){
            return $this->dateLimite;
        }

        public function getDuree(){
            return $this->duree;
        }

        public function getNom(){
            return $this->nom;
        }
        
        public function saveBDD(){
            $pdo = MaBD::getInstance();
            $requete = $pdo->query("INSERT INTO projet (dateDebut, dateLimite, description, nom, public) VALUES ('".$this->dateDebut."','".$this->dateLimite."','".$this->description."','".$this->nom."','".$this->public."')");
            $requete->fetchAll(PDO::FETCH_ASSOC);
        }
    }


    
?>