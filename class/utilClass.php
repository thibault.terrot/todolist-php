<?php
    require_once "header.php";
    require_once "MaBD.php";
    require_once "membreClass.php";

    Class Util{
        public static function getAllPublicProject(){
            $pdo = MaBD::getInstance();
            $requete = $pdo->query("select idCompte, nom, description, dateDebut, dateLimite, public FROM projet where public = True");
            return $requete;
        }
        public static function getAllPublicTask(){
            $pdo = MaBD::getInstance();
            $array_tasks = [];
            $requete = $pdo->query("select * FROM tache where public = True");
            foreach ($requete as $row){
                $membre = Util::getMembre($row['createur_id']);
                $task = new Tache($row['nom'], $row['description'], $row['dateDebut'], $row['dateLimite'], $row['date_creation'], $row['public'], $membre, $row['projet_id']);
                array_push($array_tasks, $task);
            }
            return $array_tasks;
        }
        public static function getProjetName(int $project_id = NULL){
            if($project_id){
                $pdo = MaBD::getInstance();
                $requete = $pdo->query("select nom FROM projet where idCompte = ".$project_id." LIMIT 1");
                foreach ($requete as $project){
                    return $project['nom'];
                } 
            }
            else{
                return "Aucun projet";
            }
        }
        public static function getCurrentMembre(){
            if(isset($_SESSION['idMembre']))
            {
                $pdo = MaBD::getInstance();
                $requete = $pdo->query("select * FROM membre where idMembre = ".$_SESSION['idMembre']." LIMIT 1");
                foreach ($requete as $row){
                    $membre = new Membre($row['idMembre'], $row['nom'], $row['identifiant'], $row['role']);
                }
                return $membre;
            }
            else return False;
        }

        public static function getMembre(int $idMembre){
            $pdo = MaBD::getInstance();
            $requete = $pdo->query("select * FROM membre where idMembre = ".$idMembre." LIMIT 1");
            foreach ($requete as $row){
                $membre = new Membre($row['idMembre'], $row['nom'], $row['identifiant'], $row['role']);
            }
            return $membre;
        }
    }


    
?>