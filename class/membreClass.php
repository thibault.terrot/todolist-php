<?php
    require_once "class/tacheClass.php";
    require_once "class/utilClass.php";

    Class Membre{
        public function __construct(int $idMembre, string $nom, string $identifiant, string $role){
            $this->idMembre = $idMembre;
            $this->nom = $nom;
            $this->identifiant = $identifiant;
            $this->role = $role;
        }

        private $idMembre = "";
        private $nom = "";
        private $identifiant = "";
        private $role = "";

        public function getId(){
            return $this->idMembre;
        }
        public function getIdentifiant(){
            return $this->identifiant;
        }
        public function getNom(){
            return $this->nom;
        }
        public function getRole(){
            return $this->role;
        }
        public function getMyTask(){
            $array_tasks = [];
            $pdo = MaBD::getInstance();
            $requete = $pdo->query("select * FROM tache where createur_id = ".$_SESSION['idMembre']);
            foreach ($requete as $row){
                $task = new Tache($row['nom'], $row['description'], $row['dateDebut'], $row['dateLimite'], $row['date_creation'], $row['public'], Util::getCurrentMembre(), $row['projet_id']);
                array_push($array_tasks, $task);
            }
            return $array_tasks;
        }
        public function getMyProject(){

            return $this->role;
        }
    }


    
?>