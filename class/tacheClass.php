<?php
    require_once "MaBD.php";

    Class Tache{
        public function __construct(string $nom, string $description, string $dateDebut, string $dateLimite, string $dateCreation, bool $public, Membre $createur, int $projet = NULL){
            $this->description = $description;
            $newDateDebut = date("Y-m-d", strtotime($dateDebut));  
            $this->dateDebut = $newDateDebut;
            $newDateLimite = date("Y-m-d", strtotime($dateLimite));  
            $this->dateLimite = $newDateLimite;
            $this->dateCreation = $dateCreation;
            $this->nom = $nom;
            $this->public = $public;
            $this->projet = $projet;
            $this->createur = $createur;
        }

        public $idTache = "";
        public $nom = "";
        public $public = False;
        public $description = "";
        public $image = "";
        public $dateDebut = "";
        public $dateLimite = "";
        public $duree = "";
        public $projet = NULL;
        public $createur;
        public $dateCreation;
        #private $membres[] = "";

        public function getId(){
            return $this->idTache;
        }

        public function getDescription(){
            return $this->description;
        }

        public function getDateDebut(){
            if($this->dateDebut == "1970-01-01") return "<i class=\"fa-solid fa-calendar-xmark\"></i>";
            else{
                $newDateDebut = date("d/m/Y", strtotime($this->dateDebut));
                return $newDateDebut;
            }
        }

        public function getDateLimite(){
            if($this->dateLimite == "1970-01-01") return "<i class=\"fa-solid fa-calendar-xmark\"></i>";
            else{
                $newDateLimite = date("d/m/Y", strtotime($this->dateLimite));
                return $newDateLimite;
            }
        }

        public function getDateCreation(){
            $newDateCreation = date("d/m/Y", strtotime($this->dateCreation));  
            return $newDateCreation;
        }

        public function getDuree(){
            return $this->duree;
        }

        public function getNom(){
            return $this->nom;
        }

        public function getCreateur(){
            return $this->createur;
        }

        public function getProjet(){
            if($this->projet)
                return $this->projet;
            else return "Aucun projet";
        }
        

        public function saveBDD(){
            $pdo = MaBD::getInstance();
            $createur = $this->createur;
            $createur_id = $createur->getId();
            if($this->projet) $requete = $pdo->query("INSERT INTO tache (dateDebut, dateLimite, description, nom, public, projet_id, createur_id) VALUES ('".$this->dateDebut."','".$this->dateLimite."','".$this->description."','".$this->nom."','".$this->public."','".$this->projet."','".$createur_id."')");
            else $requete = $pdo->query("INSERT INTO tache (dateDebut, dateLimite, description, nom, public, projet_id, createur_id) VALUES ('".$this->dateDebut."','".$this->dateLimite."','".$this->description."','".$this->nom."','".$this->public."', NULL,'".$createur_id."')");
            $requete->fetchAll(PDO::FETCH_ASSOC);
        }
    }


    
?>