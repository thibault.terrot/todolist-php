<?php
    include "header.php";
    include "menu_todolist.php";
    require_once "autoload.php";
    include "class/tacheClass.php";
?>

<html>
    <body>
        <div class="container">
            <h1>To-Do List</h1>
            
            <br/>
            <br/>
            <br/>
            <table class="main-table">
                <tr><td><a href="" type="button" class="main-btn btn btn-secondary  btn-lg">Connexion <i class="fas fa-sign-in-alt"></i></a></td><td><a href="" type="button" class="main-btn btn btn-secondary  btn-lg">Inscription <i class="fas fa-plus-circle"></i></a></td></tr>
                <tr><td><a href="task.php" type="button" class="main-btn btn btn-secondary  btn-lg">Toutes les tâches <i class="fas fa-tasks"></i></a></td><td><a href="my-task.php" type="button" class="main-btn btn btn-secondary  btn-lg">Mes tâches</a></td></tr>
                <tr><td><a href="projet.php" type="button" class="main-btn btn btn-secondary  btn-lg">Les projets <i class="fas fa-project-diagram"></i></a></td><td><a href="my-projet.php" type="button" class="main-btn btn btn-secondary  btn-lg">Mes projets</a></td></tr>
            </table>
            <br/>
            <br/>
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>