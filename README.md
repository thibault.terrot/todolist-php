TO-DO List

TO-DO List permet de créer une liste de tâches à éffectuer. Chaque tâches dispose d'un statut: Non effectué, effectué.

Par le click d'un bouton, la tâche pourra passer de l'un à l'autre des statuts.

Les tâches seront modifiables. Ont aura la possibilité d'importer des images dans la description de la tâche.

Un système de compte pourra être mis en place, ainsi plusieurs collaborateurs identifié avec des comptes différents pourront gérer leur propres tâches et voir celles des autres.

Idées en vrac:
 - Syteme pour associer une tâche à un membre
 - Ajouter un "projet", le projet pourra contenir plusieurs tâches
 - Ajouter une date limite pour la tâche
 - Ajouter une date de début pour la tâche
 - Ajouter une durée pour la tâche
 - Les tâches pourront être visible en mode "calendrier".
 - Possibilité de rendre visible ou cacher une tâches aux autres membres
