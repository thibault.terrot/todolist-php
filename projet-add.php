<?php
    include "header.php";
    include "menu_todolist.php";
    require_once __DIR__ . "/autoload.php";
    include "class/projetClass.php";

    $Err = array();
    if (empty($_POST["nom"])) {
        $Err[] = "- Veuillez remplir le nom du projet.";
    } else {
        $nom = $_POST["nom"];
    }
    if (empty($_POST["description"])) {
        $description = False;      
    } else {
        $description = $_POST["description"];
    }
    if (empty($_POST["dateDebut"])) {
        $dateDebut = False;
    } else {
        $dateDebut = $_POST["dateDebut"];
    }
    if (empty($_POST["dateLimite"])) {
        $dateLimite = False;
    } else {
        $dateLimite = $_POST["dateLimite"];
    }
    if (empty($_POST["projet"])) {
        $projet = False;
    } else {
        $projet = $_POST["projet"];
    }
    if (empty($_POST["membre"])) {
        $membre = False;
    } else {
        $membre = $_POST["membre"];
    }
    if (empty($_POST["public"])) {
        $public = False;
    } else {
        $public = $_POST["public"];
    }
    
    if(count($Err)==0){ //Aucune erreur
        $projet = new Projet($nom , $description, $dateDebut, $dateLimite, $public);
        $save = $projet->saveBDD();
        require("task.php");

        echo "<br/>Projet ajouté avec succès.<br/><br/>";
    }
    else{
        $erreurs = "";
        foreach ($Err as $er){
            $erreurs .="</br>&nbsp;&nbsp;&nbsp;&nbsp;".$er;
        }
        echo "<div class='alert alert-danger' role='alert'>Le rpojet n'a pas pu être ajouté: ".$erreurs."</div>";
    }

?>
