<?php
    include "header.php";
    include "menu_todolist.php";
    require_once __DIR__ . "/autoload.php";
?>

<html>
    <body>
        <div class="container">
            <h1>Vos projets</h1>
            <br/>
            <div class="alert alert-info" role="alert">
                Ici sont affichées vos projets. Tous les projets auxquels vous avez été assigné.
            </div>
            <br/>

            <table class="project-table">
            </table>
            <br/>
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>