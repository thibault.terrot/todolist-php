<?php
/* Faire un fichier autoload séparé évite d'avoir à modifier tous les fichiers de tests si on décide de changer
 * la politique d'auto chargement (comme simplement le nom du répertoire où sont les classes).
 *
 * On utilise ici deux fonctions autoload pour chercher les classes soit dans Classes soit dans ../Classes
 * L'intérêt est qu'on peut lancer les tests depuis n'importe quel répertoire :
 *      php Tests/testContacts.php
 * ou si on est dans Tests :
 *      php testContacts.php
 */

spl_autoload_register(function ($className) {
    @include __DIR__."/class/$className.php";
});
spl_autoload_register(function ($className) {
    @include __DIR__."/../class/$className.php";
});
