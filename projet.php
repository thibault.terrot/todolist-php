<?php
    include "header.php";
    include "menu_todolist.php";
    include "class/utilClass.php";
?>

<html>
    <body>
        <div class="container">
            <h1>Tous les projets</h1>
            <br/>
            <div class="alert alert-info" role="alert">
                Ici sont affichées tous les projets publiques. Les projets rendus privés par leurs créateurs ne sont pas affichés.
            </div>
            <br/>

            <table class="table table-striped  project-table">
            <tr><th>Nom</th><th>Description</th><th>Date de début</th><th>Date de fin</th></tr>
            <?php
                $requete = Util::getAllPublicProject();
                foreach ($requete as $row){
                    if($row['dateDebut'] == "1970-01-01"){
                        $row['dateDebut'] = "";
                    } 
                    if($row['dateLimite'] == "1970-01-01"){
                        $row['dateLimite'] = "";
                    }
                    echo '<tr><td>'.$row['nom'].'</td><td>'.$row['description'].'</td><td>'.$row['dateDebut'].'</td><td>'.$row['dateLimite'].'</td></tr>';
                }
            ?>
            </table>
            <br/>
            <br/>
            <?php 
                if(isset($_SESSION['user']))
                {
                    echo '<h1>Ajouter un projet</h1>
                    <form action="projet-add.php" method="POST" enctype="multipart/form-data">
                        <p>Nom: <input type="text" name="nom" /></p>
                        <input style="display:none;" type="text" name="idMembre" value="'.$_SESSION['idMembre'].'"/>
                        <p>Publique: <input type="checkbox" name="public" /></p>
                        <p>Description: <br /><textarea name="description" rows="10"
                        cols="50"></textarea></p>
                        <input type="date" name="dateDebut" value="'.date('d/m/Y').'">
                        <input type="date" name="dateLimite" value="'.date('d/m/Y').'">
                        <br/><br/>
                        <button type="submit" class="btn btn-primary">Créer tâche <i class="fas fa-plus-square"></i></button>
                    </form>';
                }else{
                    echo '<div class="alert alert-info" role="alert">Veuillez vous connecter pour créer un projet</div>';
                }
            ?>
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>