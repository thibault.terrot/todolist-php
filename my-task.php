<?php
    include "header.php";
    include "menu_todolist.php";
    require_once "class/tacheClass.php";
    require_once "class/utilClass.php";
?>

<html>
    <body>
        <div class="container">
            <h1>Vos tâches</h1>
            <br/>
            <div class="alert alert-info" role="alert">
                Ici sont affichées vos tâches. Toutes les tâches auxquelles vous avez été assigné.
            </div>
            <br/>
            <?php
                if(isset($_SESSION['user']))
                {
                    $current_user = Util::getCurrentMembre();
                    $array_task = $current_user->getMyTask();
                    echo '<table class="table table-striped task-table"><tr><th>Nom</th><th>Description</th><th>Date de début</th><th>Date de fin</th><th>Date de création</th><th>Projet</th></tr>';
                    foreach ($array_task as $task){
                        echo '<tr><td>'.$task->getNom().'</td><td>'.$task->getDescription().'</td><td>'.$task->getDateDebut().'</td><td>'.$task->getDateLimite().'</td><td>'.$task->getDateCreation().'</td><td>'.$task->getProjet().'</td></tr>';
                    }
                    echo '</table>';
                }
                else{
                    echo 'Veuillez vous connecter pour pouvoir voir vos tâches';
                }
            ?>
            <br/>
        </div>
        <?php
            include "footer.php";
        ?>
    </body>
</html>