<html>
    <nav id="main_menu_top" class="navbar navbar-expand-lg navbar-dark bg-dark">                    
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/index.php"><i class="fas fa-home"></i> Accueil</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-tasks"></i> Tâches
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="nav-link" href="my-task.php">Mes tâches </a>
                        <a class="nav-link" href="task.php">Toutes les tâches </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-project-diagram"></i> Projets
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="nav-link" href="my-projet.php">Mes projets </a>
                        <a class="nav-link" href="projet.php">Tous les projets </a>
                    </div>
                </li>
                <?php
                    if(isset($_SESSION['user']))
                    {
                        echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user"></i> Bienvenu, '.$_SESSION['user'].'
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="nav-link" href=""> Compte </a>
                                    <hr/>
                                    <form class="nav-link" id="deco" action="Connexion.php" method="post">
                                        <button class="btn btn-secondary" type="submit" name="Déconnexion">Déconnexion</button>
                                    </form>
                                </div>
                            </li>';
                    }
                    else{
                        echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user"></i> Connectez-vous
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="nav-link" href="Connexion.php"><i class="fas fa-sign-in-alt"></i> Connexion </a>
                                    <a class="nav-link" href=""><i class="fas fa-plus-circle"></i> Inscription </a>
                                </div>
                            </li>';
                    }
                ?>
                    
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Liens utiles
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="nav-link" href="https://gitlab.com/thibault.terrot/todolist-php">GitLab</a>
                    </div>
                </li>
            </ul>
    </nav>
</html>